﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Objects
{
    public class CountryStat
    {
        public string Country { get; set; }
        public int StoresNumber { get; set; }
        public double MinPrice { get; set; }
    }
}
