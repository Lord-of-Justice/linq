﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linq.Objects;

namespace Linq
{
    public static class Tasks
    {
        #region low
        public static IEnumerable<string> Task2(char c, IEnumerable<string> stringList)
        {
            return stringList.Where(x => x.StartsWith(c) && x.EndsWith(c) && x.Length > 1);
        }

        public static IEnumerable<string> Task3(IEnumerable<string> stringList)
        {
            return stringList.OrderBy(x => x.Length);
        }

        public static IEnumerable<string> Task4(IEnumerable<string> stringList)
        {
            return stringList.Select(x => x.First().ToString() + x.Last());
        }
        public static IEnumerable<string> Task6(int k, IEnumerable<string> stringList)
        {
            return stringList.Where(x => x.Length == k && char.IsDigit(x.Last())).OrderBy(x => x);
        }
        public static IEnumerable<string> Task8(IEnumerable<int> integerList)
        {
            string[] stringList = new string[integerList.Count()];
            integerList = integerList.OrderBy(x => x);
            int i = 0;
            foreach (var num in integerList)
            {
                if (num % 2 != 0)
                {
                    stringList[i] = num.ToString();
                    i++;
                }
            }
            string[] result = new string[i];
            i = 0;
            foreach (var num in stringList)
            {
                if (num != null)
                {
                    result[i] = num;
                    i++;
                }
            }
            return result;
            
        }
        #endregion

        #region middle
        public static IEnumerable<string> Task1(IEnumerable<int> numbers, IEnumerable<string> stringList)
        {
            return numbers.Select(n => stringList.FirstOrDefault(x => char.IsDigit(x[0]) && x.Length == n) ?? "Not found");
        }

        public static IEnumerable<int> Task5(int k, IEnumerable<int> integerList)
        {
            var res = integerList.Where(x => x % 2 == 0).Except(integerList.Skip(k)).Reverse();
            return res;
        }

        public static IEnumerable<int> Task7(int k, int d, IEnumerable<int> integerList)
        {
            var res = integerList
                .TakeWhile(x => x <= d)
                .Concat(integerList.Skip(k))
                .OrderByDescending(x => x);
            return res;

        }

        public static IEnumerable<string> Task9(IEnumerable<string> stringList)
        {
            var res = stringList
                .GroupBy(x => x.FirstOrDefault())
                .Select(g => g.Aggregate(
                    (0, g.Key),
                    (agg, next) => (agg.Item1 + next.Length, g.Key)))
                .OrderByDescending(x => x.Item1).ThenBy(x => x.Key)
                .Select(x => $"{x.Item1}-{x.Key}");
            return res;

        }

        public static IEnumerable<string> Task10(IEnumerable<string> stringList)
        {
            var res = stringList
                .OrderBy(str => str)
                .GroupBy(str => str.Length)
                .Select(g => g.Aggregate("", (s, s1) => s + char.ToUpper(s1.Last())))
                .OrderByDescending(s => s.Length);
            return res;

        }
        #endregion

        #region advanced
        public static IEnumerable<YearSchoolStat> Task11(IEnumerable<Entrant> nameList)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<string> Task12(IEnumerable<int> integerList1, IEnumerable<int> integerList2)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<YearSchoolStat> Task13(IEnumerable<Entrant> nameList, IEnumerable<int> yearList)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<MaxDiscountOwner> Task14(IEnumerable<Supplier> supplierList,
                IEnumerable<SupplierDiscount> supplierDiscountList)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<CountryStat> Task15(IEnumerable<Good> goodList, 
            IEnumerable<StorePrice> storePriceList)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
