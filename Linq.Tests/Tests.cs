using NUnit.Framework;
using System.Collections.Generic;

namespace Linq.Tests
{
    public class Tests
    {
        [Test]
        public void Task8Test()
        {
            foreach (var data in Task8Data())
            {
                Task8(data.integerList, data.expected);
            }
        }

        private void Task8(IEnumerable<int> integerList, IEnumerable<string> expectedResult)
        {
            var actualResult = Tasks.Task8(integerList);
            Assert.AreEqual(expectedResult, actualResult);
        }

        private IEnumerable<(IEnumerable<int> integerList, IEnumerable<string> expected)> Task8Data()
        {
            yield return (
                integerList: new[] { 140, 45, -14, 0, -15, 147 },
                expected: new[] { "-15", "45", "147" });
            yield return (
                integerList: new[] { 190, 56, 47, -5681, -45, -89652, 7893 },
                expected: new[] { "-5681", "-45", "47", "7893" });
            yield return (
                integerList: new[] { 93, 45, 789, 456, -45, 78, 235, -896, 4563 },
                expected: new[] { "-45", "45", "93", "235", "789", "4563" });
        }

        [Test]
        public void Task2Test()
        {
            foreach (var data in Task2Data())
            {
                Task2(data.c, data.stringList, data.expected);
            }
        }
        private void Task2(char c, IEnumerable<string> stringList, IEnumerable<string> expectedResult)
        {
            var actualResult = Tasks.Task2(c, stringList);
            Assert.AreEqual(expectedResult, actualResult);
        }

        private IEnumerable<(char c, IEnumerable<string> stringList, IEnumerable<string> expected)> Task2Data()
        {
            yield return (
                c: 'a',
                stringList: new[] { "a fkgfjkgfgmk a", "a", "adddddd", "ddddddda" },
                expected: new[] { "a fkgfjkgfgmk a" });
            yield return (
                c: 'g',
                stringList: new[] { "gf", "g", "gdddddd", "dddddddg" },
                expected: new List<string>());
            yield return (
                c: 'B',
                stringList: new[] { "BETA", "BETA B", "Be or not to be", "some sentence with B" },
                expected: new[] { "BETA B" });
            yield return (
                c: '+',
                stringList: new[] { "c++", "+-+", "+6987/", "+478+" },
                expected: new[] { "+-+", "+478+" });
        }




    }
}